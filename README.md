# Releases
[v1.4](https://bitbucket.org/SilicaAndPina/activate.vpk/downloads/activate-v1.4.vpk) - Icon got Copystriked by Freakler, and added option to NOT backup to pd0 if you dont want to.  
[v1.3](https://bitbucket.org/SilicaAndPina/activate.vpk/downloads/activate-v1.3.vpk) - Added an option to restore activation data if cannot be found in tm0:/activate     
[v1.2](https://bitbucket.org/SilicaAndPina/activate.vpk/downloads/activate-v1.2.vpk) - Now backs up from NVS and allows time to be set before 1/1/2015 (but vita checks elsewhere anyway)      
[v1.1](https://bitbucket.org/SilicaAndPina/activate.vpk/downloads/activate-v1.1.vpk) - Added support for Development Kit Consoles             
[v1.0](https://bitbucket.org/SilicaAndPina/activate.vpk/downloads/activate.vpk) - Inital Release- Activates testkit's and backs up act.dat / actsig.dat.           


# testkit/devkit-activator
re-activate your test/dev kit vita after it expires  

only works if you have your activation files still  
tm0:activate/act.dat & tm0:activate/actsig.dat & 0x520 in NVS.

I attempted to bypass the 1/1/2015 restriction on setting the srtc but it seems anything before it
does *technically* change but is considered an invalid time when being read.
(this doesnt matter on devkit, only a minor annoyance on soft resets!)

upon running activate.vpk your activation data will be backed up to pd0:/data/ and ux0:/data/ its highly recommended to make a copy on your PC as well.  

pd0:data/ because its read-only and it remains even after updating or reinstalling the firmware. basicly it would be difficult to actidently delete them

oh btw, normal henkaku doesnt work if your system is activated, (it assumes its allready installed)
ive made the modifications nessorcary to make it install regardless and hosting it currently at http://vitatricks.tk/henkek

thanks zecoaxco for saving me from RE'ing ksceRtcSetCurrentNetworkTick
  
Download: https://bitbucket.org/SilicaAndPina/activate.vpk/downloads/activate-v1.4.vpk

NOTE: IF YOU USED THIS BEFORE ON 1.1 OR LOWER I HIGHLY RECOMMEND RUNNING IT AGAIN ON 1.2+
AS IT BACKS UP THE ACTIVATION TOKEN IN NVS AS WELL NOW. WHICH IS *REQUIRED* FOR A PROPER RESTORE OF ACTIVATION DATA.
     
(If you are missing NVS data but still have act.dat and actsig.dat- you can generate the NVS data with [act-gen-nvs](https://bitbucket.org/SilicaAndPina/gen-act-nvs/src/master/))     