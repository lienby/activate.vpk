// SilicaAndPina
// userland <-> userland bridge
// WHY TF DO I HAVE TO DO THIS ?

#include <psp2/kernel/modulemgr.h>
#include <taihen.h>
#include <vitasdk.h>
#include "rtcKernelBridge.h"

int silRtcSetCurrentTick(unsigned int timestamp1, unsigned int timestamp2)
{
    return ksilRtcSetCurrentTick(timestamp1,timestamp2);
}

int silRtcSetCurrentNetworkTick(unsigned int timestamp1, unsigned int timestamp2)
{
    return ksilRtcSetCurrentNetworkTick(timestamp1,timestamp2);
}

int silRtcSetCurrentSecureTick(unsigned int timestamp1, unsigned int timestamp2)
{
    return ksilRtcSetCurrentSecureTick(timestamp1,timestamp2);
}

int silSblPostSsMgrSetCpRtc(unsigned int timestamp)
{
    return ksilSblPostSsMgrSetCpRtc(timestamp);
}

int silDumpNvsAct(unsigned int fileno)
{
    return ksilDumpNvsAct(fileno);
}
int silRestoreNvsAct(unsigned int fileno)
{
    return ksilRestoreNvsAct(fileno);
}
int silIoMount(int id, int permission)
{
	return ksilIoMount(id, permission);
}
int silIoUmount(int id, int force)
{
	return ksilIoUmount(id, force);
}

void _start() __attribute__ ((weak, alias ("module_start")));
int module_start(SceSize argc, const void *args) {
  return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args) {
  return SCE_KERNEL_STOP_SUCCESS;
}
