#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <psp2/ctrl.h>
#include <psp2/kernel/threadmgr.h>
#include <sys/time.h>
#include <taihen.h>
#include <vitasdk.h>

#include "graphics.h"
#include "rtcUserBridge.h"

#define printf psvDebugScreenPrintf


int ret = 0x00;
SceUID fd = 0x00;
int backup_pd0 = 0x00;

int get_key(void) {
	static unsigned buttons[] = {
		SCE_CTRL_SELECT,
		SCE_CTRL_START,
		SCE_CTRL_UP,
		SCE_CTRL_RIGHT,
		SCE_CTRL_DOWN,
		SCE_CTRL_LEFT,
		SCE_CTRL_LTRIGGER,
		SCE_CTRL_RTRIGGER,
		SCE_CTRL_TRIANGLE,
		SCE_CTRL_CIRCLE,
		SCE_CTRL_CROSS,
		SCE_CTRL_SQUARE,
	};

	static unsigned prev = 0;
	SceCtrlData pad;
	while (1) {
		memset(&pad, 0, sizeof(pad));
		sceCtrlPeekBufferPositive(0, &pad, 1);
		unsigned newb = prev ^ (pad.buttons & prev);
		prev = pad.buttons;
		for (int i = 0; i < sizeof(buttons)/sizeof(*buttons); ++i)
			if (newb & buttons[i])
				return buttons[i];

		sceKernelDelayThread(1000); // 1ms
	}
}

int WriteFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 0777);
	if (fd < 0)
		return fd;

	int written = sceIoWrite(fd, buf, size);

	sceIoClose(fd);
	return written;
}

int ReadFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file,SCE_O_RDONLY, 0777);
	if (fd < 0)
		return fd;

	int read = sceIoRead(fd, buf, size);

	sceIoClose(fd);
	return read;
}

int getFileSize(const char *file) {
	SceUID fd = sceIoOpen(file, SCE_O_RDONLY, 0);
	if (fd < 0)
		return fd;
	int fileSize = sceIoLseek(fd, 0, SCE_SEEK_END);
	sceIoClose(fd);
	return fileSize;
}

int CopyFile(char *src, char *dst)
{
	int size = getFileSize(src);
	if (size <= -1){
		printf("getFileSize Failed, ret: 0x%x\n",size);
	}
	else
	{
	char *buffer = malloc(size);
	ret = ReadFile(src,buffer,size);
	if (ret <= -1)
		{
			printf("ReadFile failed 0x%x\n",ret);
		}
		
	ret = WriteFile(dst,buffer,size);
	if (ret <= -1){
			printf("WriteFile failed 0x%x\n",ret);
		}
	}
	return ret;
}

int restore_act()
{
	printf("Activation Data not Found!\nChecking for backup...");
	int is_ux0 = getFileSize("ux0:/data/act.dat") >= 0;
	int is_pd0 = getFileSize("pd0:/data/act.dat") >= 0;

	sceIoMkdir("tm0:/activate", 0777);
	
	if( (is_ux0) || (is_pd0) )
	{
		printf("Activation Data backup found!\n");
		printf("Press any key to restore (existing activation data will be overwritten)\n");
		get_key();
		if(is_ux0)
		{
			CopyFile("ux0:/data/act.dat","tm0:/activate/act.dat");
			CopyFile("ux0:/data/actsig.dat","tm0:/activate/actsig.dat");
			CopyFile("ux0:/data/act.dat","tm0:/activate/act.dat");
			ret = silRestoreNvsAct(0x1);
		}
		else if(is_pd0)
		{
		
			CopyFile("pd0:/data/act.dat","tm0:/activate/act.dat");
			CopyFile("pd0:/data/actsig.dat","tm0:/activate/actsig.dat");
			CopyFile("pd0:/data/act.dat","tm0:/activate/act.dat");
			ret = silRestoreNvsAct(0x0);
		}
		
		// Check Success
		if(ret <= -1)
		{
			printf("ksceSblNvsWriteData failed 0x%x",ret);
		}
		else
		{
			printf("Activation data restored!");
			get_key();
			sceKernelExitProcess(0);
		}
	}
	else
	{
		printf("No backup data found.\n");
		printf("if you have a backup of Activation Data please place it in ux0:/data\n\n");
		printf(" - ux0:/data/act.dat\n");
		printf(" - ux0:/data/actsig.dat\n");
		printf(" - ux0:/data/act-nvs.dat\n");
				
		get_key();
		sceKernelExitProcess(0);
	}
	return ret;
}



int backup_act()
{
	printf("Backing up activation files...\n");
	
	
	
	if(backup_pd0){
		silIoUmount(0xC00, 0); 
		silIoUmount(0xC00, 1); 
		silIoMount(0xC00, 2);
	}
	sceIoMkdir("ux0:/data", 0777);
	if(backup_pd0)
		sceIoMkdir("pd0:/data", 0777);
	
	//backup actdat
	CopyFile("tm0:/activate/act.dat","ux0:/data/act.dat");
	if(backup_pd0)
		CopyFile("tm0:/activate/act.dat","pd0:/data/act.dat");
	
	//backup actsig
	CopyFile("tm0:/activate/actsig.dat","ux0:/data/actsig.dat");
	if(backup_pd0)
		CopyFile("tm0:/activate/actsig.dat","pd0:/data/actsig.dat");
	
	if(backup_pd0)
	{
		ret = silDumpNvsAct(0x0);
		if (ret <= -1){
				printf("ksceSblNvsReadData failed 0x%x\n",ret);
		}
	}
	ret = silDumpNvsAct(0x1);
	if (ret <= -1){
			printf("ksceSblNvsReadData failed 0x%x\n",ret);
	}
	
	if(backup_pd0)
	{
		silIoUmount(0xC00, 0); 
		silIoUmount(0xC00, 1); 
		silIoMount(0xC00, 1);
	}
	printf("Done, backup at ");
	if(backup_pd0)
		printf("pd0:/data and ");
	printf("ux0:/data\nPLEASE KEEP THESE FILES SAFE!!!\n\n");
	return ret;
}

void main() {

	psvDebugScreenInit();
	
	if(vshSblAimgrIsCEX())
	{
		printf("This is not a test/dev kit . . . \n");
		get_key();
		sceKernelExitProcess(0);
	}
	
	char kplugin_path[0x200];
	memset(kplugin_path,0x00,0x200);
	
	char uplugin_path[0x200];
	memset(uplugin_path,0x00,0x200);
	
	char titleid[12];
	sceAppMgrUmount("app0:");
	
	printf("Loading kmodule...");
	
	sceAppMgrAppParamGetString(0, 12, titleid , 256);
	sprintf(kplugin_path, "ux0:app/%s/kern_clockset.skprx", titleid);
	int kernel_modid = 0;
	kernel_modid = taiLoadStartKernelModule(kplugin_path, 0, NULL, 0);
	
	
	if(kernel_modid <= 0 && kernel_modid != 0x8002d013 && kernel_modid != 0x8002d004)
	{
		printf("Error cannot load kern_clockset.skprx error 0x%x\n",kernel_modid);
		get_key();
		sceKernelExitProcess(0);
	}
	
	printf("OK\n");
	
	printf("Loading umodule...");
	
	sceAppMgrAppParamGetString(0, 12, titleid , 256);
	sprintf(uplugin_path, "ux0:app/%s/user_clockset.suprx", titleid);
	int user_modid = 0;
	user_modid = sceKernelLoadStartModule(uplugin_path, 0, NULL, 0, NULL, NULL);
	
	
	if(user_modid <= 0)
	{
		printf("Error cannot load user_clockset.suprx error 0x%x\n",user_modid);
		get_key();
		sceKernelExitProcess(0);
	}
	
	printf("OK\n");
	
	//Check act data exists
	
	ret = getFileSize("tm0:/activate/act.dat");
	if(ret <= -1)
	{
		restore_act();
	}
	
	ret = getFileSize("tm0:/activate/actsig.dat");
	if(ret <= -1)
	{
		restore_act();
	}
	
	//Read Exp Date
	
	printf("Finding expiration start date...");
	
	unsigned int startDate;
	fd = sceIoOpen("tm0:/activate/act.dat",SCE_O_RDONLY, 0444);
	if(fd <= -1)
	{
		printf("Failed to read act.dat 0x%x",fd);
	}
	
	sceIoLseek(fd,0xC,SCE_SEEK_SET);
	sceIoRead(fd,&startDate,0x04);
	sceIoClose(fd);
	
	printf(" found: %x\n",startDate);
	printf("\n\nWould you like to backup activation data to pd0:/data\nThis area is Read-Only and is not touched by system updates\nso it would be very hard to accidentally remove it\nX: Yes Backup to pd0:/data\nO: No just backup to ux0:/data\n");
	
	int button = get_key();
	if(button == SCE_CTRL_CROSS)
		backup_pd0 = 1;
	else
		backup_pd0 = 0;
	
	backup_act();
	
	if(vshSblAimgrIsTool()) //devkit
	{
		printf("Updating CPRTC to %x\n",startDate);
		ret = silSblPostSsMgrSetCpRtc(startDate);
		sceRegMgrSetKeyInt("/CONFIG/DATE", "set_automatically", 0);
		printf("has ret 0x%x\n",ret);
	}
	else if(vshSblAimgrIsDEX()) //testkit
	{
		
		unsigned long long int timestamp = startDate;
		timestamp = ((timestamp * 1000) + 62135596800000) * 1000;
		printf("PSTIME: %llx\n",timestamp);

		if(startDate <= 0x54A48E00)
		{
			printf("Warn: Start Date is Before 1/1/2015\n");
			printf("It's impossible to set the time before 1/1/2015.\n");
			printf("However i can cause an overflow when reading to make it *appear* as 1/1/2015\n");
			printf("doing this will break trophy earning though.\n");
			printf("\nPress any key to continue\n");
			
			get_key();
			timestamp += 0xAFEE8AFE9EEEF1FFull; // a really big number.
			
		}
		
		printf("Creating splits..\n");
	
		unsigned int split1 = timestamp >> 32;
		unsigned int split2 = timestamp & 0xffffffff;

		
		printf("Split1: %x\n",split1);
		printf("Split2: %x\n",split2);
		
		printf("Updating SecureTick.. ");
		ret = silRtcSetCurrentSecureTick(split2,split1);
		printf("has ret 0x%x\n",ret);
		
		sceRegMgrSetKeyInt("/CONFIG/DATE", "set_automatically", 0);			
	}

        
	if(ret == 0)
	{
		printf("\n\nYour PSVITA Test/Dev Kit has been activated!\n");
		printf("\n");
		printf("Deactivate by holding START + SELECT + PS + POWER\n\n");
		printf("Press any key to turn off console");
		get_key();
		scePowerRequestStandby();
	}
	else
	{
		printf("Error: 0x%x\n",ret);
	}
	
	get_key();
}
