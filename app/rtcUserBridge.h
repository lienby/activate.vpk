int silRtcSetCurrentTick(unsigned int timestamp1, unsigned int timestamp2);
int silRtcSetCurrentNetworkTick(unsigned int timestamp1, unsigned int timestamp2);
int silRtcSetCurrentSecureTick(unsigned int timestamp1, unsigned int timestamp2);
int silSblPostSsMgrSetCpRtc(unsigned int timestamp);
int silDumpNvsAct(unsigned int fileno);
int silRestoreNvsAct(unsigned int fileno);
int silIoMount(int id, int permission);
int silIoUmount(int id, int force);